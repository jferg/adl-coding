import { BaseEntityColumns } from "./base-entity";
import { Person } from "../models/person";
import { EntitySchema } from "typeorm";

/**
 * TypeORM definitions for Person type.  This represents a generic person,
 * which can have a role of 'USER' (aka Canvasser) or 'CONTACT'.
 */
export const PersonEntity = new EntitySchema<Person>({
  name: "person",

  columns: {
    ...BaseEntityColumns,
    firstName: {
      type: "varchar",
      length: 50,
    },
    lastName: {
      type: "varchar",
      length: 50,
    },
    email: {
      type: "varchar",
      length: 100,
    },
    role: {
      type: "varchar",
      length: 10,
    },
  },
  relations: {
    /* contactsMade: {
      type: "one-to-many",
      target: "entry",
      inverseSide: "user",
    },
    contactsReceived: {
      type: "one-to-many",
      target: "entry",
      inverseSide: "contact",
    }, */
  },
});
