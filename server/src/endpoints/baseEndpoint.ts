import { Base } from "../models/base";
import Koa, { Context, Next } from "koa";
import Router from "@koa/router";
import BodyParser from "koa-bodyparser";
import { getConnection, EntityTarget, DeepPartial } from "typeorm";
import { v4 } from "uuid";

/**
 * BaseEndpoint is a rudimentary implementation of a RESTful CRUD endpoint for Koa.js.
 *
 * It can be applied to any TypeORM entity - provide it an entity type and the path at
 * which the endpoint for that entity type should be mounted. It will create PUT, GET, POST, and
 * DELETE actions on that endpoint which can be used for CRUD actions.
 *
 * It implements no access control and only minimal error handling.
 */
export class BaseEndpoint<T extends Base> {
  private router: Router = new Router();

  /**
   * Creates a new endpoint instance for a typeORM entity.
   *
   * @param objectType The typeORM entity type which the endpoint will deal with.
   * @param basePath The path under which the endpoint should be mounted in the Koa app.
   */
  constructor(public objectType: EntityTarget<T>, public basePath: string) {}

  /**
   * Registers the endpoint with a Koa.js application instance.
   *
   * @param app The Koa.js application instance.
   */
  public registerEndpoint(app: Koa) {
    this.router.use(BodyParser());
    this.router.get(`/${this.basePath}/:id?`, this.read.bind(this));
    this.router.delete(`/${this.basePath}/:id`, this.delete.bind(this));
    this.router.put(`/${this.basePath}`, this.create.bind(this));
    this.router.post(`${this.basePath}/:id`, this.update.bind(this));

    app.use(this.router.routes()).use(this.router.allowedMethods());
  }

  /**
   * Koa handler for creating a new object.
   *
   * @param ctx
   * @param next
   */
  async create(ctx: Context, next: Next): Promise<void> {
    const toCreate = Object.assign({}, ctx.request.body.data);
    if (toCreate) {
      if (toCreate.id) {
        ctx.status = 400;
        ctx.message =
          "Cannot provide `id` for object to be created.  To upsert, use POST method.";
        return;
      } else {
        toCreate.id = v4();
        const result: T = await getConnection().manager.save(
          this.objectType,
          toCreate
        );
        ctx.status = 200;
        ctx.body = result;
      }
    } else {
      ctx.status = 400;
      ctx.message = "Bad Request";
    }
  }

  /**
   * Koa handler for reading an object or list of objects.
   *
   * @param ctx
   * @param next
   */
  async read(ctx: Context, next: Next): Promise<void> {
    let result;
    if (ctx.params.id) {
      // Specific ID was requested
      result = await getConnection().manager.find(this.objectType, {
        id: ctx.params.id,
      });
    } else {
      // Return listing.
      if (ctx.query) {
        result = await getConnection().manager.find(
          this.objectType,
          Object.assign({}, ctx.query)
        );
      } else {
        result = await getConnection().manager.find(this.objectType);
      }
    }
    ctx.body = result;
    ctx.status = 200;
  }

  /**
   * Koa handler for updating an object.
   *
   * @param ctx
   * @param next
   */
  async update(ctx: Context, next: Next): Promise<void> {
    const updates: DeepPartial<T> = ctx.request.body;
    const result = await getConnection().manager.update(
      this.objectType,
      ctx.params.id,
      updates
    );
    if (result.affected === 0) {
      ctx.status = 404;
      ctx.message = "Not Found";
    }
    ctx.status = 200;
    ctx.body = await getConnection().manager.find(this.objectType, {
      id: ctx.params.id,
    });

    return;
  }

  /**
   * Koa handler for deleting an object.
   *
   * @param ctx
   * @param next
   */
  async delete(ctx: Context, next: Next): Promise<void> {
    if (ctx.params.id) {
      const result = await getConnection().manager.delete(
        this.objectType,
        ctx.params.id
      );
      if (result.affected === 0) {
        ctx.status = 404;
        ctx.body = "Not Found";
      }
    }
  }
}
