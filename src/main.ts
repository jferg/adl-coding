import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { Quasar } from "quasar";
import { QuasarUserOptions } from "@/quasar-user-options";

createApp(App).use(Quasar, QuasarUserOptions).use(router).mount("#app");
