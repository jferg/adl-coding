import { PersonStore } from "@/store/person-store";

/**
 * Implementation of Base Store class for storing Users (Canvassers).
 */
export class UsersStore extends PersonStore {}

export const usersStore = new UsersStore("USERS");
