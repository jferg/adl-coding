import { reactive, readonly } from "vue";

/**
 * Base Store - Extremely simple reactive datastore base class.
 */

// eslint-disable-next-line
export abstract class Store<T extends Object> {
  protected state: T;

  constructor(readonly storeName: string) {
    const data = this.data();
    this.state = reactive(data) as unknown as T;
  }

  protected abstract data(): T;

  public getState(): T {
    return readonly(this.state) as T;
  }
}
