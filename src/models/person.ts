import { Base } from "./base";

/**
 * A generic Person object - type determined by the "Role" attribute.
 */

export enum PersonRole {
  USER = "USER",
  CONTACT = "CONTACT",
}

export interface Person extends Base {
  firstName: string;
  lastName: string;
  email: string;
  role: PersonRole;
  // contactsMade: Entry[];
  // contactsReceived: Entry[];
}
