export * from "./person-service";

export enum ServiceIdentifiers {
  "PERSON_SERVICE" = "personService",
}
