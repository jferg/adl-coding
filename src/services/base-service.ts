import { Base } from "@/models/base";
import axios from "axios";

/**
 * Simple implementation of a REST API access service using axios.  Does not
 * yet implement update functionality.
 */
export class BaseService<T extends Base> {
  constructor(public baseUrl: string, public path: string) {}

  /**
   * Fetches a single object of type T by objectId.
   *
   * @param id The objectId of the object to fetch.
   * @returns Promise<T|undefined> The requested object, or undefined if there is no such object.
   * @throws ServiceError If an error occurs querying the object.
   */
  public async fetchOne(id: string): Promise<T | undefined> {
    // TODO - This should probably be refactored to use Path.join and do checking for well-formedness of the URL.
    const result = await axios.get(`${this.baseUrl}${this.path}/${id}`);
    if (result.status === 200) {
      return result as unknown as T;
    } else if (result.status === 404) {
      return undefined;
    } else {
      throw new ServiceError(
        `Error fetching requested object. (${result.status}: ${result.statusText})`
      );
    }
  }

  /**
   * Fetches objects of type T matching criteria.  Matching logic is limited and simplistic.
   *
   * @param criteria An object of Partial<T> with the attributes to match against.
   * @returns Promise<Array<T>> An array (may be empty) of matching objects.
   * @throws ServiceError If an error occurs querying the object(s).
   */
  public async fetchMany(criteria: Partial<T>): Promise<Array<T>> {
    const result = await axios.get(`${this.baseUrl}${this.path}`, {
      params: criteria,
    });
    if (result.status === 200) {
      return result.data as unknown as T[];
    } else if (result.status === 404) {
      return [];
    } else {
      throw new ServiceError(
        `Error fetching all objects. (${result.status}: ${result.statusText})`
      );
    }
  }

  /**
   * Fetches all records of type T from the API.  Simply calls fetchMany with no filter.
   *
   * @returns Array<T> If query is successful, an array of T objects is returned.  (If
   * there are no objects, an empty array is returned.)
   * @throws ServiceError If query fails.
   */
  public async fetchAll(): Promise<Array<T>> {
    return this.fetchMany({});
  }

  /**
   * Creates a single object of type T using the API.
   *
   * @param object A partial T object.
   * @returns Promise<T> The created object.
   * @throws ServiceError If an error occurs creating the object.
   */
  public async createOne(object: Partial<T>): Promise<T> {
    const result = await axios.put(`${this.baseUrl}${this.path}`, {
      data: object,
    });
    if (result.status === 200) {
      return result.data as unknown as T;
    } else {
      throw new ServiceError(
        `Error creating object. (${result.status}: ${result.statusText})`
      );
    }
  }

  /**
   * Attempts to delete one object of type T from the API.
   *
   * @param id The objectId of the object to delete.
   * @returns Promise<T|false> On successful delete, the deleted object is returned.
   * @throws ServiceError If delete fails (i.e. object doesn't exist).
   */
  public async deleteOne(id: string): Promise<T> {
    const result = await axios.delete(`${this.baseUrl}${this.path}/${id}`);
    if (result.status === 200) {
      return result.data as unknown as T;
    } else {
      throw new ServiceError(
        `Error deleting object. (${result.status}: (${result.status}: ${result.statusText})`
      );
    }
  }
}

export class ServiceError extends Error {}
