module.exports = {
  pluginOptions: {
    quasar: {
      importStrategy: "kebab",
      rtlSupport: false,
    },
  },
  transpileDependencies: ["quasar"],
  devServer: {
    proxy: {
      "^/entry": {
        target: "http://localhost:3000/",
        ws: true,
        changeOrigin: true,
      },
      "^/person": {
        target: "http://localhost:3000/",
      },
    },
  },
};
