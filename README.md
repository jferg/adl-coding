
## Local Startup
```
npm install
npm run build
cd server
npm install
npm start
```

## Development Mode Startup

Both pieces need to be running for the app to work.  

### Frontend
```
npm install
npm run serve
```
### Backend
```
cd server
npm install
npm run dev
```

The application will be accessible at http://localhost:8080/.

## Docker
```
docker build . -t my-app
docker run -p 3000:3000 my-app
```

The application will be accessible at http://localhost:3000/.

### Run tests
```
cd server
npm test
```

(There currently are no frontend tests.)

